var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

// разделение главного массива на два разных
var points = studentsAndPoints.filter(function (value, index) {
  return index % 2 != 0;
});

var students = studentsAndPoints.filter(function (value, index) {
  return index % 2 == 0;
});


// вывести список студентов без использования циклов
console.log('Список студентов:');
students.forEach(function (value, index) {
  console.log('Студент ' + value + ' набрал(а) ' + points[index] + ' баллов');
});


// найти студента, набравшего наибольшее количество баллов
var max, student;

points.forEach(function (value, index) {
  if (!max || value > max) {
    max = value;
    student = students[index];
  }
});

console.log('Студент набравший максимальный балл: %s (%d баллов)', student, max);


// увеличить баллы студентам "Ирина Овчинникова" и "Александр Малов"

var newArray = points.map(function (value, index) {
  switch (students[index]) {
    case 'Ирина Овчинникова':
      return value + 30
      break;
    case 'Александр Малов':
      return value + 30
      break;
    default:
      return value
  }
});

points = newArray;
